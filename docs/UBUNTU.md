# Installing docker-master on Ubuntu Server 16.04 LTS

Note: this should work on any Debian based Linux distribution.

```

# apt-get update
# apt-get install linux-image-extra-$(uname -r) linux-image-extra-virtual
# apt-get install apt-transport-https ca-certificates
# apt-key adv --keyserver hkp://ha.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
# REPO_URL="deb https://apt.dockerproject.org/repo ubuntu-xenial main"
# echo ${REPO_URL} | tee /etc/apt/sources.list.d/docker.list
# apt-get update
# apt-get install docker-engine
```

