let imports = {
    'BaseObject' : require(__commonPath + '/lib/BaseObject.class.js'),
};

module.exports = class Util extends imports.BaseObject {
    static GetCurrentTimestamp() {
        return Math.floor(Date.now() / 1000);
    }
};
