'use strict';

let imports = {
    "fs": require("fs"),
    "request": require("request"),
    "BaseObject" : require(__commonPath + '/lib/BaseObject.class.js'),
};

module.exports = class AppConfig extends imports.BaseObject {
    constructor(args) {
        super();
        this._config = {};
        this.on('updateConfig', (prefix, newConfig) => this.onUpdateConfig(prefix, newConfig));
    }

    getParameter(key) {
        let splits = key.split(".");
        let obj = this._config;
        if (splits[0]!='local' && splits[0]!='remote') {
            splits.unshift('local');
        }
        while(splits.length) {
            obj = obj[splits.shift()];
            if (obj instanceof Function) {
                obj = obj();
            }
            if (!obj) {
                return null;
            }
        }
        return obj;
    }

    onUpdateConfig(prefix, newConfig) {
        // TODO: check to ensure config doesn't change the "type" of a sub-key
        // Overwrite, but only things which are missing
        this._config[prefix] = Object.assign(this._config[prefix] || {}, newConfig);
    }

    updateConfigFromFile(prefix, configFile) {
        if (!configFile) {
            configFile = prefix;
            prefix = 'local';
        }
        let config = require(configFile);
        if (config) {
            return this.emit('updateConfig', prefix, config);
        };
    }

    updateConfigFromUrl(prefix, configUrl) {
        if (!configUrl) {
            configUrl = prefix;
            prefix = 'remote';
        }
        imports.request.get(configUrl, (error, response, body) => {
            if (error) {
                this.logger.error(`Could not get config from ${this.configUrl}: ${error}`);
                return;
            }
            if (response.statusCode === 200) {
                let config = JSON.parse(body);
                if (config) {
                    return this.emit('updateConfig', prefix, config);
                }
            }
        });
    }
};
