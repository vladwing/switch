'use strict';

let imports = {
    "EventEmitter" : require('events').EventEmitter,
};

module.exports = class BaseObject extends imports.EventEmitter {
    constructor() {
        super();

        // make this class abstract
        if (this.constructor === module.exports) {
            throw new Error('BaseObject class is an abstract class!');
        }
    }
    // Static properies inherited by all objects
    // the logger
    get logger() { return module.exports._logger; }
    static set logger(logger) { this._logger = logger; }
    static get logger() { return this._logger; }

    // the stats manager
    get stats() { return module.exports._stats; }
    static set stats(stats) { this._stats = stats; }
    static get stats() { return this._stats; }

    // basic event wrapper
    emit() {
        super.emit.apply(this, arguments);
    }

    /**
     * Logger forward methods
     * Useful if we want signed log messages ( like adding reuqest ID )
     */
    error(...args) {
        return this.logForward("error", ...args);
    }
    warn(...args) {
        return this.logForward("warn", ...args);
    }
    info(...args) {
        return this.logForward("info", ...args);
    }
    debug(...args) {
        return this.logForward("debug", ...args);
    }
    trace(...args) {
        return this.logForward("trace", ...args);
    }

    /**
     * Should be ovewritten in the child classes
     */
    logForward(method, ...args) {
        return this.logger[method].apply(this.logger, [this.uniqueId].concat(...args));
    }
};
