'use strict';

let imports = {
    "colors": require("colors"),
    "argv" : require('argv'),
    "fs" : require('fs'),
    "express" : require('express'),
    "query" : require('connect-query'),
    "bodyParser": require('body-parser'),
    "httpStatus" : require('http-status-codes'),
    "App" : require(__commonPath + '/lib/App.class.js'),
};

module.exports = class BackendApp extends imports.App {
    constructor(args) {
        super(args);

        // API needs to be initialized first
        this._serviceReady = false;
        this.canStart = false;
        this.started = false;
    }

    initWeb() {
        this.webApp = imports.express();
        this.webApp.set('views', __commonPath + '/resources/views');
        this.webApp.set('view engine', 'twig');

        this.webApp.set('twig options', {
            strict_variables: true
        });

        // init routes
        let routingFile = __modulePath + '/resources/routing/routing.js';
        if (imports.fs.existsSync(routingFile)) {
            // apply the routes
            require(routingFile)(this);

            this.logger.info('Imported module routing file:', routingFile);
        } else {
            this.logger.warn('Module routing file does not exist:', routingFile);
        }

        // api can be started now
        this.canStart = true;

        this.onApiInit();
    }

    /**
     * Default API init listener
     * This method should be overwritten in the module class
    **/
    onApiInit() {
        this.logger.error("Api is ready to be booted!");
        this.logger.error("This handler should be overwritten");
    }

    /**
     * Get the serviceReady flag value
    **/
    get serviceReady() {
        return this._serviceReady;
    }

    /**
     * Set the serviceReady flags
    **/
    set serviceReady(avail) {
        this._serviceReady = !!avail;
        if (this._serviceReady){
            this.logger.info("Service enabled");
        } else {
            this.logger.info("Service disabled");
        }
    }

    /**
     * Starts the API
    **/
    start() {
        if (!this.canStart) {
            throw new Error('Api not initialized! run initApi() first!');
        }
        if (this.started){
            throw new Error('Api already started');
        }

        this.started = true;

        let port = this.config.getParameter('api.port');
        let host = this.config.getParameter('api.host');
        this.logger.info('Starting API on port', port);
        let server = this.webApp.listen(port, host);
        server.on('error', (e) => {
            if (e.code === 'EADDRINUSE') {
                this.logger.error("Another service is already using this port:", e);
            } else {
                this.logger.error("HTTP server unexpected error:", e);
            }

            // fatal error
            throw e;
        });
    }
};
