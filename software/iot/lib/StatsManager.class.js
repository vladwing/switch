'use strict';

let imports = {
    "util" : require("util"),
};

module.exports = class StatsManager {
    constructor(statsd) {
        this.statsd = statsd;
        this.prefixes = [];
    }
    setLogger(logger) {
        this.logger = logger;
    }
    addPrefix(prefix) {
        this.prefixes.push(prefix);
    }
    statsCallback(error, bytes, statsData){
        let totalTime = (new Date()).getTime() - statsData.timeStart;
        if (error) {
            this.logger.error("Stats send error", error, "in", totalTime, "ms");
        }
        if (statsData.callback) {
            return statsData.callback(error, bytes);
        }
    }
    getStatsArguments(...inputArgs) {
        let statsData = {
            'timeStart' : (new Date()).getTime(),
            'arguments' : inputArgs,
            'callback' : null,
        };
        let args = Array.prototype.slice.call(inputArgs);
        // set the correct key
        args[0] = this.getStatsKey(args[0]);
        // set the correct callback
        if (args[args.length - 1] instanceof Function) {
            statsData.callback = args.pop();
        }
        args.push((error, bytes) => this.statsCallback(error, bytes, statsData));
        return args;
    }
    getStatsKey(key) {
        if (!this.prefixes.length) {
            return key;
        }
        let prefixFull = '';
        for (let i=0; i<this.prefixes.length; i++) {
            let prefixStr = '';
            if (imports.util.isFunction(this.prefixes[i])) {
                prefixStr = this.prefixes[i]();
            } else {
                prefixStr = this.prefixes[i];
            }
            prefixFull += prefixStr + '.';
        }
        return prefixFull + key;
    }

    cleanKeyPart(key) {
        key = String(key) || '';
        return key.replace(/[\.\s]+/g, '-').replace(/[\-]+/g, '-').replace(/(^[\-]+)|([\-]+$)/g, '');
    }

    increment(key, value) {
        let args = this.getStatsArguments(key, value);
        this.logger.trace("Stats increment:", args.slice(0, -1));
        this.statsd.increment(...args);
    }
    timing(key, value) {
        let args = this.getStatsArguments(key, value);
        this.logger.trace("Stats timing:", args.slice(0, -1));
        this.statsd.timing(...args);
    }
    decrement(key, value) {
        let args = this.getStatsArguments(key, value);
        this.logger.trace("Stats decrement:", args.slice(0, -1));
        this.statsd.decrement(...args);
    }
    gauge(key, value) {
        let args = this.getStatsArguments(key, value);
        this.logger.trace("Stats gauge:", args.slice(0, -1));
        this.statsd.gauge(...args);
    }
};
