'use strict';

let imports = {
    "App": require(__commonPath + '/lib/App.class.js'),
};

module.exports = class WorkerApp extends imports.App {
    initWorker() {
        this.onInitWorker();
    }
};
