'use strict';

let imports = {
    "colors": require("colors"),
    "argv" : require('argv'),
    "fs" : require('fs'),
    "path" : require('path'),
    "tmp" : require('tmp'),
    "util" : require('util'),
    "statsd" : require('node-statsd'),
    "os" : require('os'),
    "BaseObject" : require(__commonPath + '/lib/BaseObject.class.js'),
    "StatsManager" : require(__commonPath + '/lib/StatsManager.class.js'),
    "Logger" : require(__commonPath + '/lib/Logger.class.js'),
    "AppConfig": require(__commonPath + '/lib/AppConfig.class.js'),
};

module.exports = class App extends imports.BaseObject {
    constructor(args) {
        args = args || {};

        super();

        this.extraConfigPaths = args.extraConfigPaths || [];

        this.configClass = args.configClass || imports.AppConfig;

        // set the env var (app, env, etc)
        this.initCli();

        // init the config
        this.initConfig();

        // set the logger on all objects
        this.initLogger();

        // set the stats manager on all objects
        this.initStats();
    }

    initConfig() {
        this.config = new this.configClass();
        this.config.updateConfigFromFile(__commonPath + "/resources/configs/config.js");
        this.config.updateConfigFromFile(__modulePath + "/resources/configs/config.js");
    }

    /**
     * Initialized the global application logger
     * This logger should be used in all classes instead of "console.log"
    **/
    initLogger() {
        imports.BaseObject.logger = new imports.Logger({
            'name' : 'main',
            'level' : this.args.options['log-level'] || 'trace',
            'colors' : {
                "trace": imports.colors.gray,
                "debug": imports.colors.gray,
                "info": imports.colors.green,
                "warn": imports.colors.yellow,
                "error": imports.colors.red,
            },
        });
        // disable logging if specified
        this.logger.setEnabled(!this.args.options['quiet']);

        this.logger.info("Logger initialized");
    }

    /**
     * Initialized the global application stats manager
     * This stats manager should be used in all classes
    **/
    initStats() {
        let statsd = new imports.statsd();
        let stats = new imports.StatsManager(statsd);
        stats.setLogger(imports.BaseObject.logger);

        // set the default stats manager for all objects
        imports.BaseObject.stats = stats;

        // set the default stats manager for all objects
        this.stats.addPrefix(() => {
            if (!imports.os.hostname()) {
                throw new Error('No hostname found');
            }
            return imports.os.hostname().replace(/[\.]+/g, '-');
        });

        this.logger.info("Statsd initialized");
    }

    /**
     * Return the cli options for this app
    **/
    getArgvOptions() {
        return [
            {
                name: 'quiet',
                type: 'boolean',
                short: 'q',
                description: 'Disables logging',
            },
            {
                name: 'log-level',
                type: 'string',
                short: 'L',
                description: 'Set the desired log level - overwrites config value; Overwrited config value if specified',
            },
        ];
    }

    /**
     * Initializes the default CLI parameters
     * - app
     * - env
    **/
    initCli() {
        this.argv = imports.argv;

        let options = this.getArgvOptions();
        options.forEach((option) => {
            this.argv.option(option);
        });

        this.args = this.argv.run();
    }
};
