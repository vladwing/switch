'use strict';

let imports = {
    'BaseObject' : require(__commonPath + '/lib/BaseObject.class.js'),
};

// module specific app
module.exports = class CollectorFactory extends imports.BaseObject {
    constructor(args) {
        super();
        this.classes = args;
    }
    newCollector(type, config) {
        if (!this.classes[type]) {
            throw new Error('Invalid collector type');
        }
        return new this.classes[type](config);
    }
};
