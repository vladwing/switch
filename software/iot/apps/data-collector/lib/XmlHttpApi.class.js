'use strict';

let imports = {
    "request": require("request"),
    "url": require('url'),
    "xml2js": require('xml2js'),
    'BaseObject' : require(__commonPath + '/lib/BaseObject.class.js'),
};

// module specific app
module.exports = class XmlHttpApi extends imports.BaseObject {
    static get OK() { return 0; }
    static get UNKNOWN_ERROR() { return -1; }
    static get INVALID_SESSION() { return -2; }
    static get AUTHENTICATION_ERROR() { return -3; }
    static get HTTP_ERROR() { return -4; }
    static get INVALID_RESPONSE() { return -5; }

    constructor(url) {
        super();
        this.url = url;
        this.parser = imports.xml2js.parseString;
    }
    get(callback){
        imports.request.get({
            uri: this.url,
            timeout: 600,
            }, (error, response, body) => {
            if (error) {
                return callback({
                    code: XmlHttpApi.HTTP_ERROR,
                    error: JSON.stringify(error),
                });
            }
            if (response.statusCode != 200) {
                this.logger.trace("Server returned an error", response);
                let code = XmlHttpApi.UNKNOWN_ERROR;
                if (response.statusCode == 400) {
                    code = XmlHttpApi.INVALID_SESSION;
                } else if (response.statusCode == 403) {
                    code = XmlHttpApi.AUTHENTICATION_ERROR;
                }
                return callback({
                    code: code,
                    message: `Server returned statusCode ${response.statusCode}`
                });
            }
            this.parser(response.body, (err, result) => {
                if (err) {
                    this.logger.trace("Server response XML parse error", err);
                    return callback({
                        code: XmlHttpApi.XML_PARSE_ERROR,
                        message: "Server response XML parse error",
                    });
                }
                if (!result.response) {
                    return callback({
                        code: XmlHttpApi.INVALID_RESPONSE,
                        message: "Server response doesn't contain the expected response"
                    });
                }
                return callback(null, result.response);
            });
        });
    }
};
