'use strict';

let imports = {
    'moment': require('moment-timezone'),
    'Util': require(__commonPath + '/lib/Util.class.js'),
    'XmlHttpApi' : require(__modulePath + '/lib/XmlHttpApi.class.js'),
    'BaseObject' : require(__commonPath + '/lib/BaseObject.class.js'),
};

// module specific app
module.exports = class AdconCollector extends imports.BaseObject {
    constructor(args) {
        super();
        this.name = args.name || 'adcon';
        this.url = args.url;
        this.user = args.user;
        this.password = args.password;
        this.map = args.map;
        this.timezone = args.timezone;
        this.sessionKey = null;
        this.sessionKeyAge = null;
        this.maxSessionKeyAge = 30 * 60;
    }
    // URLs
    getLoginUrl(){
        return this.url + `?function=login&user=${this.user}&passwd=${this.password}`;
    }
    getDataUrl(idString){
        return this.url + `?function=getdata&session-id=${this.sessionKey}&id=${idString}&date=${this.getDate()}&slots=1000`;
    }
    getDate() {
        imports.moment.tz.setDefault(this.timezone);
        let then = imports.moment(new Date()).subtract(20, "minutes");
        return imports.moment(then).format("YYYYMMDD[T]hh:mm:ss");
    }
    // Internals
    updateSessionKey(callback){
        let url = this.getLoginUrl();
        new imports.XmlHttpApi(url).get((error, response) => {
            if (error) {
                return callback(error);
            }
            this.sessionKey = response.result[0].string[0];
            this.sessionKeyAge = imports.Util.GetCurrentTimestamp();
            this.logger.debug(`Storing new session key: ${this.sessionKey}`);
            return callback(null);
        });
    }
    shouldRenewSession(){
        // if (!this.sessionKeyAge) {
        //     this.logger.trace('Session key age is not set');
        // }
        // if (!this.sessionKey) {
        //     this.logger.trace('Session key is not set');
        // }
        // if ((imports.Util.GetCurrentTimestamp() - this.sessionKeyAge) > this.maxSessionKeyAge) {
        //     this.logger.trace(`Session key has expired: ${imports.Util.GetCurrentTimestamp() - this.sessionKeyAge} > ${this.maxSessionKeyAge} - ${(imports.Util.GetCurrentTimestamp() - this.sessionKeyAge) > this.maxSessionKeyAge}`);
        // }
        return !this.sessionKeyAge || !this.sessionKey || (imports.Util.GetCurrentTimestamp() - this.sessionKeyAge) > this.maxSessionKeyAge;
    }
    parseDate(dateString, previous) {
        if (dateString.startsWith('+') && previous) {
            return previous + parseInt(dateString);
        }
        if (dateString.startsWith('+')) {
            throw new Error('Invalid timestamp sequence. Missing a first date in the list.');
        }
        // 20180315T21:00:00
        // 01234567890123456
        imports.moment.tz.setDefault(this.timezone);
        return imports.moment(dateString, "YYYYMMDD[T]HH:mm:ss").unix();
    }
    parseDataObject(data) {
        let results = [];
        let previous = null;
        for (let nodeElement of data.node) {
            let nodeID = parseInt(nodeElement['$']['id']);
            for (let valueElement of nodeElement.v) {
                let timestamp = this.parseDate(valueElement['$']['t'], previous);
                previous = timestamp;
                let value = parseFloat(valueElement['_']);
                let result = {
                    timestamp: timestamp,
                    key: this.map[nodeID] || 'Unknown',
                    collector: this.name,
                    value: value,
                };
                // this.logger.trace("Result XML: ", result);
                results.push(result);
            }
        }
        return results;
    }
    // Public
    getLatestValues(callback) {
        if (this.shouldRenewSession()) {
            return this.updateSessionKey((error) => {
                if (error) {
                    this.logger.error("Error receiving session key:", error);
                    return callback(error, null);
                }
                return this.getLatestValues(callback);
            });
        }
        let nodeIdString = Object.keys(this.map).join(";");
        let url = this.getDataUrl(nodeIdString);
        new imports.XmlHttpApi(url).get((error, response) => {
            if (error) {
                if (error.code == imports.XmlHttpApi.INVALID_SESSION) {
                    this.sessionKey = null;
                    this.sessionKeyAge = null;
                    return this.getLatestValues(callback);
                } else {
                    this.logger.error("Error querying addUPI:", error);
                    return callback(error);
                }
            }
            let parsedResponse = this.parseDataObject(response);
            return callback(null, parsedResponse);
        });
    }
};
