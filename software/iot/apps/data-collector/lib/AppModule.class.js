'use strict';

let imports = {
    "fs" : require('fs'),
    'WorkerApp' : require(__commonPath + '/lib/WorkerApp.class.js'),
    'RecurrentJob' : require(__commonPath + '/lib/RecurrentJob.class.js'),
    'CollectorFactory' : require(__modulePath + '/lib/CollectorFactory.class.js'),
    // TODO: remove this
    'AlertBackend' : require(__modulePath + '/lib/backends/AlertBackend.class.js'),
    'AlertAction' : require(__modulePath + '/lib/backends/Alert/AlertAction.class.js'),
    'AlertTrigger' : require(__modulePath + '/lib/backends/Alert/AlertTrigger.class.js'),
    'DefaultTrigger' : require(__modulePath + '/lib/backends/Alert/DefaultTrigger.class.js'),
};

// module specific app
module.exports = class AppModule extends imports.WorkerApp {
    constructor(args) {
        super(args);

        // allow requests to reach the controllers
        this.serviceReady = true;
        this.initWorker();
    }

    /**
     * Initializes worker
     */
    onInitWorker() {
        // User defined stuff here
        this.initCollectorFactory();
        this.initBackends();
        this.initJobs();
        this.initTriggers();
        this.initActions();
    }
    initCollectorFactory() {
        let factoryConfig = {};
        for (let collectorName of Object.keys(this.config.getParameter('collectors'))) {
            let collectorClassName = this.config.getParameter('collectors')[collectorName];
            // TODO: clean collectorClassName
            if (factoryConfig[collectorName]) {
                throw new Error(`Duplicate collector name ${collectorName}`);
            }
            let collectorFile = __modulePath + '/lib/collectors/' + collectorClassName + '.class.js';
            if (!imports.fs.existsSync(collectorFile)) {
                throw Error(`Couldn't load collector class from ${collectorFile}`);
            }
            let collectorClass = require(collectorFile);
            factoryConfig[collectorName] = collectorClass;
        }
        this.collectorFactory = new imports.CollectorFactory(factoryConfig);
    }
    initBackends() {
        this.backends = [];
        for (let backend of this.config.getParameter('backends')) {
            let backendFile = __modulePath + '/lib/backends/' + backend.class + '.class.js';
            if (!imports.fs.existsSync(backendFile)) {
                throw Error(`Couldn't load backend class from ${backendFile}`);
            }
            let BackendClass = require(backendFile);
            if (!BackendClass) {
                throw Error("Invalid class for the backend");
            }
            let backendInstance = new BackendClass(backend.config);
            backendInstance.setContext(this);
            if (!backendInstance) {
                throw Error("Couldn't create backend");
            }
            this.backends.push(backendInstance);
        }
    }
    initActions() {
        this.actions = {};
        this.defaultAction = new imports.AlertAction();
        for (let actionConfig of this.config.getParameter('actions')) {
            let action = new imports.AlertAction(actionConfig);
            this.actions[actionConfig.name] = action;
        }
    }
    initTriggers() {
        this.triggers = {};
        this.defaultTrigger = new imports.DefaultTrigger();
        for (let triggerConfig of this.config.getParameter('triggers')) {
            let trigger = new imports.AlertTrigger(triggerConfig);
            this.triggers[triggerConfig.name] = trigger;
        }
    }
    initJobs() {
        this.collectors = [];
        let count = 0;
        for (let jobConfig of this.config.getParameter('jobs')) {
            let job = new imports.RecurrentJob({
                name: jobConfig.name || 'Job ' + count++,
                interval: jobConfig.interval || 30000,
                handler: () => {
                    if (!this.collectors[jobConfig.name]) {
                        this.collectors[jobConfig.name] = this.collectorFactory.newCollector(jobConfig.collector,
                            Object.assign({'name': jobConfig.name}, jobConfig.config));
                    }
                    this.collectors[jobConfig.name].getLatestValues((err, data) => {
                        if (err) {
                            this.logger.error(`Job '${job.name}' got an error from the collector`, err);
                        } else {
                            for (let backend of this.backends) {
                                for (let value of data) {
                                    backend.sendValue(Object.assign({}, value));
                                }
                            }
                        }
                        return job.runFinished();
                    });
                }
            });
            job.start();
        }
    }
    getAction(actionName) {
        return this.actions[actionName] || this.defaultAction;
    }
    getTrigger(triggerName) {
        return this.triggers[triggerName] || this.defaultTrigger;
    }
};
