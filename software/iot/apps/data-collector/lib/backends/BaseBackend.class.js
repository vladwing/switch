'use strict';

let imports = {
    'BaseObject' : require(__commonPath + '/lib/BaseObject.class.js'),
};

// module specific app
module.exports = class BaseBackend extends imports.BaseObject {
    setContext(cntx){
        this.context = cntx;
    }
    get name(){
        return module.exports.name;
    }
    sendValue(data) {
    }
};
