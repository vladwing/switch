'use strict';

let imports = {
    'BaseBackend' : require(__modulePath + '/lib/backends/BaseBackend.class.js'),
};

// module specific app
module.exports = class AlertBackend extends imports.BaseBackend {
    constructor(args) {
        super();
        this.rules = args.rules;
        this.allData = {};
    }
    sendValue(data) {
        super.sendValue(data);
        this.allData[[data.collector, data.key].join('.')] = data.value;
        for (let rule of this.rules) {
            let trigger = this.context.getTrigger(rule.trigger);
            let action = this.context.getAction(rule.action);

            if (trigger.checkCondition(this.allData)) {
                action.doAction(this.allData);
            }
        }
    }
};
