'use strict';

let imports = {
    'BaseObject' : require(__commonPath + '/lib/BaseObject.class.js'),
    'Util': require(__commonPath + '/lib/Util.class.js'),
};

// module specific app
module.exports = class AlertTrigger extends imports.BaseObject {
    constructor(args) {
        super();
        this.parameter = args.parameter;
        this.operator = args.operator;
        this.value = parseFloat(args.value);
    }
    checkCondition(data) {
        let current = parseFloat(data[this.parameter]);
        if (!current) {
            return false;
        }
        if (data.timestamp + 180 < imports.Util.GetCurrentTimestamp()) {
            this.logger.trace("Old data, ignoring!");
            return false;
        }
        switch (this.operator) {
            case "<":
                if (current < this.value) {
                    this.logger.debug(`${current} < ${this.value}`);
                    return true;
                }
                break;
            case ">":
                if (current > this.value) {
                    this.logger.debug(`${current} > ${this.value}`);
                    return true;
                }
                break;
            case "<=":
                if (current <= this.value) {
                    this.logger.debug(`${current} <= ${this.value}`);
                    return true;
                }
                break;
            case ">=":
                if (current >= this.value) {
                    this.logger.debug(`${current} >= ${this.value}`);
                    return true;
                }
                break;
            case "==":
                if (current == this.value) {
                    this.logger.debug(`${current} == ${this.value}`);
                    return true;
                }
                break;
        }
        return false;
    }
};
