'use strict';

let imports = {
    'BaseObject' : require(__commonPath + '/lib/BaseObject.class.js'),
};

// module specific app
module.exports = class DefaultTrigger extends imports.BaseObject {
    checkCondition(data) {
        return false;
    }
};
