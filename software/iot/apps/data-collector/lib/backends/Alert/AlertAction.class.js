'use strict';

let imports = {
    'request': require('request'),
    'Util': require(__commonPath + '/lib/Util.class.js'),
    'BaseObject' : require(__commonPath + '/lib/BaseObject.class.js'),
};

// module specific app
module.exports = class AlertAction extends imports.BaseObject {
    constructor(args) {
        super();
        args = args || {};
        this.type = args.type;
        this.url = args.url;
        this.payload = args.payload;
        this.activeEveryMin = args.activeEveryMin;
        this.lastActionTime = null;
        this.setupAction();
    }
    setupAction() {
        switch(this.type) {
            case "http-post-json":
                this.doAction = (data) => this.doPostJson(data);
                break;
            default:
                this.doAction = () => this.logger.error("Action type is not defined");
        }
    }
    doPostJson(data) {
        if (!this.lastActionTime || (this.lastActionTime + this.activeEveryMin * 60) < imports.Util.GetCurrentTimestamp())
        {
            this.lastActionTime = imports.Util.GetCurrentTimestamp();
            this.logger.warn("Making call to operator");
            imports.request({
                'method': 'POST',
                'uri': this.url,
                'json': true,
                'body': JSON.parse(this.payload),
            }, () => {
                //this.logger.trace(`Call sent to ${this.url} for`, data, "with payload", this.payload);
            });
        }
    }
};
