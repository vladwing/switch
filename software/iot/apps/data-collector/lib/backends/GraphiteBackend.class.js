'use strict';

let imports = {
    'graphite': require('graphite'),
    'BaseBackend' : require(__modulePath + '/lib/backends/BaseBackend.class.js'),
};

// module specific app
module.exports = class GraphiteBackend extends imports.BaseBackend {
    constructor(args) {
        super();
        this.server = args.address;
        this.prefix = args.prefix;
        this.map = args.map || {};
        this.initClient();
    }
    setContext(cntx){
        this.context = cntx;
    }
    initClient() {
        this.client = imports.graphite.createClient(`plaintext://${this.server}/`);
    }
    sendValue(data) {
        super.sendValue(data);
        let key = [data.collector, data.key].join(".");
        if (this.map[key]) {
            key = this.map[key];
        }
        if (this.prefix) {
            key = [this.prefix, key].join(".");
        }
        let metrics = {};
        metrics[key] = data.value;
        this.client.write(metrics, data.timestamp * 1000, (error) => {});
    }
};
