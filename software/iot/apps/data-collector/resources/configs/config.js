module.exports = {
    'api': {
        'port': () => parseInt(process.env.SERVICE_PORT),
        'name': () => process.env.SERVICE_NAME,
    },
    'jobs': [
        {
            'name': 'RTU1',
            'interval': 20000,
            'collector': 'adcon',
            'config': {
                'timezone': 'Europe/Bucharest',
                'url': 'http://a850-uhf.beia-telemetrie.ro/addUPI',
                'user': 'addupi',
                'password': 'ADDUPI',
                'map': {
                    797: 'RelativeHumidity',
                    796: 'Temperature',
                    789: 'WaterLevel',
                },
            },
        },
        {
            'name': 'V-RTU1',
            'interval': 20000,
            'collector': 'adcon',
            'config': {
                'timezone': 'Europe/Bucharest',
                'url': 'http://rtu_emulator:8080/addUPI',
                'user': 'addupi',
                'password': 'ADDUPI',
                'map': {
                    797: 'RelativeHumidity',
                    796: 'Temperature',
                    789: 'WaterLevel',
                },
            },
        }
    ],
    'collectors': {
        'adcon': 'AdconCollector',
    },
    'triggers': [
        {
            'name': 'verifyTemperatureMax',
            'parameter': 'RTU1.Temperature',
            'operator': ">",
            'value': 40,
        },
        {
            'name': 'verifyTemperatureMin',
            'parameter': 'RTU1.Temperature',
            'operator': "<",
            'value': 10,
        },
        {
            'name': 'verifyVTemperatureMax',
            'parameter': 'V-RTU1.Temperature',
            'operator': ">",
            'value': 25,
        },
        {
            'name': 'verifyVTemperatureMin',
            'parameter': 'V-RTU1.Temperature',
            'operator': "<",
            'value': 20,
        },
    ],
    'actions': [
        {
            'name': 'call-default',
            'activeEveryMin': 3,
            'type': "http-post-json",
            'url': "http://notify/originate",
            'payload': '{"extension": "0724271640", "file": "beia/switch-warning", "sip_provider": "clickphone"}'
        },
    ],
    'backends': [
        {
            'class': 'GraphiteBackend',
            'config': {
                'address': 'graphite:2003',
                'prefix': 'beia.switch',
            },
        },
        {
            'class': 'AlertBackend',
            'config': {
                'rules': [
                    {
                        'trigger': 'verifyTemperatureMax',
                        'action': 'call-default',
                    },
                    {
                        'trigger': 'verifyTemperatureMin',
                        'action': 'call-default',
                    },
                    {
                        'trigger': 'verifyVTemperatureMax',
                        'action': 'call-default',
                    },
                    {
                        'trigger': 'verifyVTemperatureMin',
                        'action': 'call-default',
                    },
                ]
            },
        }
    ]
};
