'use strict';

let imports = {
    'BackendApp' : require(__commonPath + '/lib/BackendApp.class.js'),
};

// module specific app
module.exports = class AppModule extends imports.BackendApp {
    constructor(args) {
        super(args);

        // boot up the API
        this.initWeb();

        // allow requests to reach the controllers
        this.serviceReady = true;
    }

    /**
     * Initializes application routes and starts the http(s) server
     */
    onApiInit() {
        // User defined stuff here
        // ...
        this.start();
    }
};
