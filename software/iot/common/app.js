#!/usr/bin/env node
'use strict';

let imports = {
    "path" : require('path'),
};

// initialize the paths
require(imports.path.dirname(process.argv[1]) + '/bootstrap.js');

imports["AppModule"] = require(__modulePath + '/lib/AppModule.class.js');

// initialize the app
module.exports = new imports.AppModule({
    'extraConfigPaths' : [
        __modulePath
    ]
});
