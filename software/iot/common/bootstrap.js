'use strict';

let imports = {
    "path" : require('path'),
    "fs" : require('fs'),
};

global.__commonPath = imports.fs.realpathSync(imports.path.dirname(__filename) + '/..');
global.__modulePath = imports.path.dirname(process.argv[1]);


// add local logic here - if any
