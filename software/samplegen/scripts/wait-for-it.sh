#!/bin/sh
HOST=$1;PORT=$2;TIMEOUT=$3;END=$(($(date "+%s+$TIMEOUT")));shift 3;while [ $(date "+%s") -lt $END ]; do nc -z -w1 $HOST $PORT && break; done && $@
