#!/bin/sh
DIR=$(CDPATH= cd -- "$(dirname -- "$0")" && pwd)
set -xe

cd ${DIR}/..
mv src samplegen
cd samplegen
npm install
