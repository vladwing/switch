'use strict';
/* jshint node:true, laxcomma:true */

let imports = {
    EventEmitter: require('events'),
    zmq: require('zmq'),
    os: require('os'),
    uuid: require('uuid/v1'),
};

class Logger {
    constructor(logger, level) {
        this.logger = logger;
        this.levels ={
            "ERROR": Logger.ERROR,
            "WARN": Logger.WARN,
            "INFO": Logger.INFO,
            "DEBUG": Logger.DEBUG,
            "TRACE": Logger.TRACE,
        };
        this.level = this.levels[level.toUpperCase()] || Logger.WARN;
        this.error = (message) => this.logForward(message, "ERROR");
        this.warn = (message) => this.logForward(message, "WARN");
        this.info = (message) => this.logForward(message, "INFO");
        this.debug = (message) => this.logForward(message, "DEBUG");
        this.trace = (message) => this.logForward(message, "TRACE");
    }
    logForward(message, levelTxt) {
        if (this.level < this.levels[levelTxt]) {
            return;
        }
        this.logger.log(message, levelTxt);
    }
    static get ERROR() {
        return 10;
    }
    static get WARN() {
        return 20;
    }
    static get INFO() {
        return 30;
    }
    static get DEBUG() {
        return 40;
    }
    static get TRACE() {
        return 50;
    }
}

class BaseObject {
    get logger() { return BaseObject._logger; }
    static set logger(logger) { this._logger = logger; }
    static get logger() { return this._logger; }
}

class Probe extends BaseObject {
    constructor(name) {
        super();
        this.name = name;
        this.initMetrics();
    }
    initMetrics() {
        this.metrics = {};
        this.updated = false;
    }
    isUpdated() {
        return this.updated;
    }
    /**
    * @param {string} key - this is the metric name
    * @param {string} type - this is the type of the metric (default DOUBLE)
    * @param {string} units - this is the unit of measurement of the metric (default "")
    * @returns {null}
    */
    setMetric(key, type, units) {
        // !!! We will ignore the type & units if they are changed after being initialized !!!
        if (this.metrics[key]) {
            return this.logger.error("Metric was previously set", JSON.stringify(this.metrics));
        }
        this.metrics[key] = {
            name: key,
            type: type || "DOUBLE",
            units: units || "",
            value: null,
        };
        this.updated = true;
    }
    /**
    * @param {string} key - this is the metric name
    * @param {string} value - this is the value of the metric
    * @returns {boolean} - returns weather or not the value was actually set
    * Metric values will actually be converted to DOUBLE if their type is double
    */
    setActualValue(key, value) {
        if (!this.metrics[key]) {
            this.logger.error("Metric name unknown. Set it before with .setMetric()");
            return false;
        }
        if (this.metrics[key].type === "DOUBLE") {
            value = parseFloat(value);
        }
        this.metrics[key].value = value;
        return true;
    }
    /**
    * @param {object} parsed - this is the metric structure returned by Agent.parseMetric()
    * @param {string} value - this is the value of the metric
    * @returns {boolean} - returns weather or not the value was actually set
    */
    setValue(parsed, value) {
        if (!this.metrics[parsed.metricName]) {
            this.setMetric(parsed.metricName, "DOUBLE", parsed.unit);
        }
        return this.setActualValue(parsed.metricName, value);
    }
    getMetricCatalog() {
        let obj = {
            probeName: this.name,
            metrics: [],
        };
        for (let metricName of Object.keys(this.metrics)) {
            obj.metrics.push({
                name: this.metrics[metricName].name,
                type: this.metrics[metricName].type,
                units: this.metrics[metricName].units,
            });
        }
        return obj;
    }
    getMetricValues(timestamp) {
        let obj = {
            timestamp: "" + (timestamp * 1000),
            group: this.name,
            metrics: [],
        };
        let updated = false;
        for (let metricName of Object.keys(this.metrics)) {
            if (this.metrics[metricName].value === null) {
                continue;
            }
            obj.metrics.push({
                name: this.metrics[metricName].name,
                type: this.metrics[metricName].type,
                units: this.metrics[metricName].units,
                val: "" + this.metrics[metricName].value,
            });
            updated = true;
        }
        if (!updated) {
            return null;
        }
        return obj;
    }
    reset() {
        this.updated = false;
        for (let metricName of Object.keys(this.metrics)) {
            this.metrics[metricName].value = null;
        }
    }
}

class StaticInfoProbe extends Probe {
    constructor() {
        super("StaticInfoProbe");
    }
    initMetrics() {
        super.initMetrics();
        this.setMetric("os", "STRING", "");
        this.setActualValue("os", imports.os.release());
        this.setMetric("arch", "STRING", "");
        this.setActualValue("arch", imports.os.arch());
        this.setMetric("cpuNum", "STRING", "");
        this.setActualValue("cpuNum", "" + imports.os.cpus().length);
        this.setMetric("btime", "STRING", "");
        this.setActualValue("btime", "" + Date.now());
    }
    setValue(parsed, value) {
        this.logger.trace(JSON.stringify(parsed));
        if (["os", "arch", "cpuNum", "btime"].indexOf(parsed.metricName.toLowerCase()) < 0) {
            return false;
        }
        if (parsed.extras && parsed.extras.size > 0) {
            return super.setValue(parsed, parsed.extras[0]);
        } else {
            return super.setValue(parsed, "unknown");
        }
    }
}

const AGENT_MAX_IDLE_TIME = 30 * 1000;
const AGENT_SEND_HELLO_INTERVAL = 160 * 1000;
const AGENT_METRIC_RESPONSIVENESS = 10 * 1000;

class Agent extends BaseObject {
    constructor(key) {
        super();
        let parsed = this.parseMetricKey(key);
        this.id = parsed.containerID;
        this.ip = parsed.containerIP;
        this.updateLastHello();
        this.updateLastMetricTime();
        this.initDefaultProbes();
    }
    initDefaultProbes() {
        this.probes = {};
        for (let probeName of ["StaticInfoProbe"]) {
            let cls = Agent.getProbeClass(probeName);
            this.probes[probeName] = new cls();
        }
    }
    shouldDie() {
        return (Date.now() - this.lastMetricTime) > AGENT_MAX_IDLE_TIME;
    }
    shouldSendHello() {
        return this.areProbesUpdated() ||
                (
                    ((Date.now() - this.lastMetricTime) > AGENT_METRIC_RESPONSIVENESS) &&
                    (this.lastSentTime === null || (Date.now() - this.lastSentTime) > AGENT_SEND_HELLO_INTERVAL)
                );
    }
    areProbesUpdated() {
        for (let probeName of Object.keys(this.probes)) {
            if (this.probes[probeName].isUpdated())
            {
                return true;
            }
        }
        return false;
    }
    reset() {
        for (let probeName of Object.keys(this.probes)) {
            this.probes[probeName].reset();
        }
    }
    updateLastHello(last) {
        this.lastSentTime = last || null;
    }
    updateLastMetricTime() {
        this.lastMetricTime = Date.now();
    }
    parseMetric(key, value) {
        let parsed = this.parseMetricKey(key);
        if (Agent.getAgentID(key) != this.getAgentID()) {
            throw new Error("You are sending the metric to the wrong agent!");
        }
        if (!this.probes[parsed.probeName]) {
            let cls = Agent.getProbeClass(parsed.probeName);
            this.probes[parsed.probeName] = new cls(parsed.probeName);
            // Force resending on the next tick
            this.updateLastHello();
        }
        this.probes[parsed.probeName].setValue(parsed, value);
        this.updateLastMetricTime();
    }
    getAgentHello() {
        return {
            agentID: this.id,
            agentIP: this.ip.replace(/-/g, '.'),
        };
    }
    getMetricCatalog() {
        let obj = {
            agentID: this.id,
            agentIP: this.ip.replace(/-/g, '.'),
            agentName: this.name || this.ip.replace(/-/g, '.'),
            probes: [],
            tags: "",
        };
        for (let probeName of Object.keys(this.probes)) {
            obj.probes.push(this.probes[probeName].getMetricCatalog());
        }
        return obj;
    }
    getMetricValues(timestamp) {
        let obj = {
            events: [],
            agentID: this.id,
            agentIP: this.ip.replace(/-/g, '.'),
        };
        for (let probeName of Object.keys(this.probes)) {
            let values = this.probes[probeName].getMetricValues(timestamp);
            if (values) {
                obj.events.push(values);
            }
        }
        return obj;
    }
    static getProbeClass(probeName) {
        switch(probeName) {
            case "StaticInfoProbe": return StaticInfoProbe;
            default: return Probe;
        }
    }
    parseMetricKey(key) {
        let parts = key.split(".");
        //Agent.logger.trace(`parseMetricKey("${key}") -> ${JSON.stringify(parts)}`);
        return {
            containerID: parts[0],
            containerIP: parts[1],
            probeName: parts[2],
            metricName: parts[3],
            unit: parts[4] || Agent.defaultUnit,
            extras: parts.slice(4),
        };
    }
    getAgentID() {
        return this.id + Agent.JOIN_CHAR + this.ip;
    }
    static getAgentID(key) {
        let agent = new Agent(key);
        return agent.getAgentID();
    }

    get JOIN_CHAR() { return Agent._JOIN_CHAR; }
    static set JOIN_CHAR(value) { this._JOIN_CHAR = value; }
    static get JOIN_CHAR() { return this._JOIN_CHAR; }

    get prefix() { return Agent._prefix; }
    static set prefix(value) { this._prefix = value; }
    static get prefix() { return this._prefix; }

    get defaultUnit() { return Agent._defaultUnit; }
    static set defaultUnit(value) { this._defaultUnit = value; }
    static get defaultUnit() { return this._defaultUnit; }
}

class JCatascopiaBackend extends BaseObject {
    constructor(args) {
        super();
        this.prefix = args.prefix;
        this.server = args.server;
        this.portMetrics = args.portMetrics;
        this.portAnnounce = args.portAnnounce;
        this.default_unit = args.default_unit;
        this.AGENT_TIMEOUT = 10 * 1000;
        this.initEventHandlers(args.eventEmitter);
        this.initSocket();
        this.initQueue();
        this.initAgents();
    }
    initEventHandlers(emitter) {
        emitter.on('flush', (timestamp, metrics) => this.onFlushMetrics(timestamp, metrics));
        emitter.on('status', (cb) => cb(null, null)); // TODO: maybe do something here
    }
    initQueue() {
        this.queue = [];
        this.queueRunning = false;
    }
    initSocket() {
        this.socket = null;
        this.socketReady = false;
        let socket = imports.zmq.socket('pub');
        // socket.setsockopt(imports.zmq.ZMQ_SNDHWM, 1);
        // socket.setsockopt(imports.zmq.ZMQ_RCVHWM, 10);
        let url = this.getZMQurl(this.server, this.portMetrics);
        socket.on("message", (...args) => onMessageFromSocket(...args));
        socket.connect(url);
        this.metricsSocket = socket;
        this.socketReady = true;
    }
    initAgents() {
        this.agents = {};
    }
    getZMQurl(address, port, cb) {
        return "tcp://" + address + ":" + port;
    }
    onMessageFromSocket(...args) {
        this.logger.info(JSON.stringify(args));
    }
    onFlushMetrics(timestamp, metrics) {
        if (!this.socketReady) {
            this.logger.warn("ZMQ socket is not yet ready. Skipping metrics!");
            return;
        }
        this.logger.info("Received metrics");
        for (let key of Object.keys(metrics.gauges)) {
            if (!key.startsWith(this.prefix)) {
                continue;
            }
            let realKey = key.substr(this.prefix.length + 1);
            let agentID = Agent.getAgentID(realKey);
            if (!this.agents[agentID]) {
                this.logger.debug(`New agent ${agentID}`);
                this.agents[agentID] = new Agent(realKey);
            }
            this.agents[agentID].parseMetric(realKey, metrics.gauges[key]);
        }
        for (let agentID of Object.keys(this.agents)) {
            let agent = this.agents[agentID];
            if (agent.shouldDie()) {
                this.logger.info("Killing agent " + agent.id + " because of inactivity.");
                delete this.agents[agentID];
                continue;
            }
            if (agent.shouldSendHello()) {
                this.queueAnnouncement(agent, "AGENT.CONNECT", agent.getAgentHello(), /(OK)|(CONNECTED)/);
                this.queueAnnouncement(agent, "AGENT.METRICS", agent.getMetricCatalog(), /OK/);
            }
            this.queueMetric(agent, agent.getMetricValues(timestamp));
            agent.reset();
        }
        this.runQueues();
    }
    queueAnnouncement(agent, messageType, obj, expect) {
        this.logger.debug(`Sending ${messageType} for agent ${agent.getAgentID()}`);
        this.logger.trace("Queuing an announcement");
        this.queue.push({
            type: "announcement",
            agent: agent,
            messageType: messageType,
            obj: obj,
            expect: expect,
        });
    }
    queueMetric(agent, obj) {
        this.logger.debug("Queuing metric values");
        this.queue.push({
            type: "metric",
            agent: agent,
            obj: obj,
        });
    }
    runQueues() {
        if (this.queueRunning) {
            return this.logger.info("Queue is already running, not starting again.");
        }
        this.queueRunning = true;
        let queueCallback = (err, result) => {};
        queueCallback = (err, result) => {
            if (err) {
                this.logger.error(`Error when sending queue message: ${err.error}`);
                if (err.agent) {
                    let agent=err.agent;
                    this.queueAnnouncement(agent, "AGENT.CONNECT", agent.getAgentHello(), /(OK)|(CONNECTED)/);
                    this.queueAnnouncement(agent, "AGENT.METRICS", agent.getMetricCatalog(), /OK/);
                }
                return setTimeout(() => queueCallback(null, "OK"));
            }
            if (this.queue.length > 0) {
                let e = this.queue.splice(0, 1)[0];
                if (e.type === "announcement") {
                    this.logger.debug("Sending an announcement");
                    if (e.messageType === "AGENT.METRICS") {
                        // Get the latest version of the catalog, with all the current metrics
                        return this.sendAnnouncement(e.messageType, e.agent.getMetricCatalog(), e.expect, e.agent, queueCallback);
                    }
                    return this.sendAnnouncement(e.messageType, e.obj, e.expect, e.agent, queueCallback);
                } else { // e.type === "metric"
                    this.logger.debug("Sending a metric");
                    if (!e.agent.shouldSendHello()) {
                        return this.sendMetrics(e.obj, queueCallback);
                    } else {
                        this.queue.push(e);
                        return setTimeout(queueCallback);
                    }
                }
            } else {
                this.logger.debug("No more elements in the queues, stopping");
                this.queueRunning = false;
                return;
            }
        };
        setTimeout(() => queueCallback(null, "OK"));
    }
    sendAnnouncement(messageType, obj, expect, agent, cb) {
        let identity = imports.uuid().replace("-", ""); // TODO: ??? needs something else ???
        let socket = imports.zmq.socket('dealer');
        // socket.setsockopt(imports.zmq.ZMQ_SNDHWM, 1);
        socket.identity = identity;
        let url = this.getZMQurl(this.server, this.portAnnounce);
        let timeout = null;
        socket.on("message", (messageType, result) => {
            let res = result.toString();
            socket.unref();
            socket.close();
            clearTimeout(timeout);
            if (expect.test(res)) {
                return cb(null, "OK");
            } else {
                agent.updateLastHello(Date.now());
                return cb({
                    error: res,
                    agent: agent,
                }, null);
            }
        });
        socket.connect(url);
        let content = JSON.stringify(obj);
        this.logger.trace(content);
        socket.send(messageType, imports.zmq.ZMQ_SNDMORE);
        socket.send(content);
        timeout = setTimeout(() => {
            socket.unref();
            socket.close();
            return cb({
                error: `Timeout on connect/send for agent ${agent.getAgentID()}`,
                agent: agent,
            }, null);
        }, this.AGENT_TIMEOUT);
    }
    sendMetrics(metrics, cb) {
        let content = JSON.stringify(metrics);
        this.logger.trace(content);
        this.metricsSocket.send(content);
        return setTimeout(() => cb(null, "OK"));
    }
};

module.exports.init = function(startupTime, config, eventEmitter, logger) {
    Agent.prefix = config.jCatascopia.prefix;
    Agent.JOIN_CHAR = "#";
    Agent.defaultUnit = config.jCatascopia.defaultUnit || "number";
    BaseObject.logger = new Logger(logger, config.logging);
    let cfg = {
        prefix: config.jCatascopia.prefix,
        server: config.jCatascopia.server,
        portMetrics: config.jCatascopia.portMetrics,
        portAnnounce: config.jCatascopia.portAnnounce,
        eventEmitter: eventEmitter,
    };
    BaseObject.logger.info(`Sending data to ${cfg.server}`);
    BaseObject.logger.info(`Prefix is ${cfg.prefix}`);
    let backend = new JCatascopiaBackend(cfg);
    return true;
};
