from __future__ import print_function
from statsd import StatsClient
import math
import random
import string
import time

WORDS = ["explain", "bedroom", "watch", "free", "branch", "itchy", "average", "vigorous",
"calculate", "spiffy", "language", "undesirable", "cattle", "bubble", "successful", "ultra",
"premium", "damaged", "note", "grab", "learned", "unkempt", "imminent", "typical", "opposite",
"broad", "introduce", "spotless", "grateful", "burn", "point", "pricey", "deeply", "delirious",
"disastrous", "seal", "wave", "smoggy", "scrape", "pop", "waiting", "face", "rain", "whisper",
"absurd", "reward", "grape", "finger", "roll", "hall", "improve", "zinc", "suspect", "retire",
"pig", "quaint", "sudden", "morning", "impulse", "groovy", "volleyball", "ad hoc", "cobweb",
"careless", "plant", "frame", "common", "valuable", "plants", "judicious", "pot", "point",
"behave", "frequent", "six", "drunk", "expert", "shaky", "roomy", "statement", "disarm",
"melt", "thank", "development", "rock", "defeated", "intelligent", "angry", "bounce", "book",
"adaptable", "full", "ear", "fasten", "unable", "empty", "seashore", "consider", "partner",
"powerful", "shock", "chunky", "purple", "frightened", "star", "jobless", "stitch", "man",
"silent", "adhesive", "ignore", "hook", "relation", "amuck", "pickle", "succinct", "ajar",
"black", "run", "mend", "cruel", "practise", "store", "protest", "supply", "five", "addition",
"jar", "earsplitting", "rod", "applaud", "company", "null", "describe", "sound", "quiver", "upset",
"calculating", "gainful", "roll", "jealous", "hands", "moaning", "capable", "drop", "grey", "prevent",
"outrageous", "stranger", "grip", "wave", "oil", "explode", "impossible", "loutish", "sort", "daffy",
"disappear", "clever", "activity", "scare", "smart", "list", "clap", "admit", "remove", "pour", "brass",
"good", "regret", "high", "heartbreaking", "delicious", "pipe", "raspy", "screeching", "dam", "exultant",
"quack", "tent", "squalid", "vein", "soft", "immense", "minor", "ice", "riddle", "obsequious", "parsimonious",
"unruly", "crowded", "spell", "dusty", "sophisticated", "condemned", "chew", "weather", "rude", "pear", "tough"]

AGENT_DEATH_CHANCE = 5
AGENT_BIRTH_CHANCE = 5

def gen_id():
    return "".join(random.choice(string.digits+"abcdef") for _ in range(12))

def gen_ip(num):
    base = 10 * 256**3 + int(random.uniform(0,255)) * 256**2 + int(random.uniform(0,255)) * 256**1 + int(random.uniform(0,255))
    ip_list = []
    for n in range(num):
        n = base + n
        numbers = [(n//(256**i))%256 for i in range(int(math.ceil(math.log(n, 256))-1), -1, -1)]
        ip_list.append("-".join(str(n) for n in numbers))
    return ip_list

def generate_agents(num):
    return zip([gen_id() for n in range(num)], gen_ip(num))

def random_death(size, death_chance):
    return sum(map(lambda _: 1 if random.uniform(0, 100) > death_chance else 0, range(size) ))

def generate_metrics(num):
    return [(string.capitalize(random.choice(WORDS))+"Probe", string.capitalize(random.choice(WORDS)) + string.capitalize(random.choice(WORDS))) for n in range(num)]

def main(host, port, prefix, number_of_agents, number_of_metrics, sleep_time):
    client = StatsClient(host, port, prefix=prefix)
    agents = generate_agents(number_of_agents)
    metrics = generate_metrics(number_of_metrics)
    while True:
        random.shuffle(agents)
        survivors = random_death(len(agents), AGENT_DEATH_CHANCE)
        print("Killing %d agents" % (len(agents) - survivors))
        agents = agents[0:survivors]
        if len(agents) < number_of_agents:
            births = random_death(number_of_agents - len(agents), 100 - AGENT_BIRTH_CHANCE)
            print("Creating %d new agents" % (births))
            agents = agents + generate_agents(births)
        print("Generating %d metrics for %d agents" % (number_of_metrics, len(agents)))
        for agent in agents:
            for metric in metrics:
                client.gauge(".".join([agent[0], agent[1], metric[0], metric[1]]), int(random.uniform(1,100)))
        print("Sleeping for %d seconds" % sleep_time)
        time.sleep(sleep_time)

if __name__ == "__main__":
    import os
    host = os.environ.get("HOST", "localhost")
    port = os.environ.get("PORT", 8125)
    prefix = os.environ.get("PREFIX", "eu.switch.beia")
    num_agents = int(os.environ.get("AGENTS", 10))
    num_metrics = int(os.environ.get("METRICS", 3))
    sleep_time = 10
    main(host, port, prefix, num_agents, num_metrics, sleep_time)
