#!/usr/bin/env python
from flask import Flask, request, render_template, Response, current_app
from functools import wraps
import random
from datetime import datetime
from pytz import timezone

app = Flask(__name__)


NODE_CONFIG = {
    797: {
        'name': 'RelativeHumidity',
        'outside': False,
        'min': 25,
        'max': 30
    },
    796: {
        'name': 'Temperature',
        'outside': False,
        'min': 20,
        'max': 25
    },
    789: {
        'name': 'WaterLevel',
        'outside': False,
        'min': 0.1,
        'max': 0.5
    }
}

SESSION_ID = '3c321ba2e1ad5c796a876099a249bac2'

class NodeConfig:
    def __init__(self, name, node_id, min, max, outside):
        super(NodeConfig, self).__init__()
        self.name = name
        self.node_id = node_id
        self.min = min
        self.max = max
        self.outside = outside

    @property
    def value(self):
        if self.outside:
            return random.uniform(self.max, 10000 * self.max)
        return random.uniform(self.min, self.max)

    @property
    def id(self):
        return self.node_id

    @property
    def time(self):
        tz = timezone('Europe/Bucharest')
        return datetime.now(tz).isoformat('T').replace('-', '').split('.')[0]

class NodeConfigs:
    def __init__(self, config):
        super(NodeConfigs, self).__init__()
        self.init_nodes(config)

    def init_nodes(self, config):
        self.nodes = []
        for node_id, node_config in config.items():
            node = NodeConfig(node_config['name'], node_id, node_config['min'], node_config['max'], node_config['outside'])
            self.nodes.append(node)

    def get(self, node_id):
        for node in self.nodes:
            if node.node_id == node_id:
                return node
        return None


def returns_xml(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        r = f(*args, **kwargs)
        return Response(r, content_type='text/xml; charset=utf-8')
    return decorated_function


def addupi_error(error_message, error_code=-1):
    return render_template('error.xml', error_message=error_message, error_code=error_code)


@returns_xml
def login():
    return render_template('login.xml', session_id=SESSION_ID)


@returns_xml
def get_data():
    id_param = request.args.get('id')
    if not id_param:
        return addupi_error('ID parameter is missing.')

    ids = [int(x) for x in id_param.split(';')]
    nodes = [current_app.node_configs.get(node_id) for node_id in ids if current_app.node_configs.get(node_id)]
    return render_template('getdata.xml', nodes=nodes)


@app.route('/')
def ui():
    outside = request.args.get('outside', 'false').lower() == "true"
    node_id = request.args.get('id', None)
    if node_id:
        node = current_app.node_configs.get(int(node_id))
        if node:
            node.outside = outside
    return render_template('config_page.html', nodes=current_app.node_configs.nodes)

@app.route('/addUPI')
def addupi_router():
    function_param = request.args.get('function')
    if not function_param:
        return addupi_error('The function parameter is required.')
    if function_param == 'login':
        return login()
    if function_param == 'getdata':
        return get_data()
    return addupi_error('Required function is unimplemented in the emulator.')


if __name__ == '__main__':
    app.node_configs = NodeConfigs(NODE_CONFIG)
    app.config.update(
        TEMPLATES_AUTO_RELOAD=True
    )
    app.run(host='0.0.0.0', port=8080)
