import os
import subprocess

import jinja2

BASE_DIRECTORY = '/srv/asterisk'

SIP_ENV_VARS = ['SIP_USERNAME',
                'SIP_SECRET',
                'SIP_HOST',
                'SIP_PORT',
                'SIP_EXTENSION',
                'SIP_PEERNAME',
                ]

ARI_ENV_VARS = ['ARI_USERNAME',
                'ARI_SECRET',
                ]

EXTEN_ENV_VARS = ['SIP_EXTENSION',
                  ]

SIP_TEMPLATE = os.path.join(BASE_DIRECTORY, 'templates/sip.conf.jinja.template')
SIP_CONFIG_FILE = '/etc/asterisk/sip.conf'

ARI_TEMPLATE = os.path.join(BASE_DIRECTORY, 'templates/ari.conf.jinja.template')
ARI_CONFIG_FILE = '/etc/asterisk/ari.conf'

EXTEN_TEMPLATE = os.path.join(BASE_DIRECTORY, 'templates/extensions.conf.jinja.template')
EXTEN_CONFIG_FILE = '/etc/asterisk/extensions.conf'


class MissingEnvVar(Exception):
    def __init__(self, variable):
        self.required_var = variable

    def __str__(self):
        return '{} environment variable is required and not set.'.format(self.required_var)


def usage():
    unique_vars = get_set_of_unique_values(SIP_ENV_VARS,
                                           ARI_ENV_VARS,
                                           EXTEN_ENV_VARS,
                                           )
    print('{} environment variables are required to run this script.'.format(unique_vars))


def get_set_of_unique_values(*iterables):
    result_set = set()
    for item in iterables:
        result_set.update(item)
    return result_set


def render(tpl_path, context):
    path, filename = os.path.split(tpl_path)
    return jinja2.Environment(
        loader=jinja2.FileSystemLoader(path or './')
    ).get_template(filename).render(context)


def get_env_var(var_name, default=None):
    result = os.environ.get(var_name, default)
    if not result:
        raise MissingEnvVar(var_name)
    return result


def generate_config_file(template_path, config_path, env_var_list):
    context = {k: get_env_var(k) for k in env_var_list}
    result = render(template_path, context)
    with open(config_path, 'w') as f:
        f.write(result)


def main():
    try:
        print('Generate sip.conf...')
        generate_config_file(SIP_TEMPLATE, SIP_CONFIG_FILE, SIP_ENV_VARS)
        print('Generate ari.conf...')
        generate_config_file(ARI_TEMPLATE, ARI_CONFIG_FILE, ARI_ENV_VARS)
        print('Generate extension.conf...')
        generate_config_file(EXTEN_TEMPLATE, EXTEN_CONFIG_FILE, EXTEN_ENV_VARS)
    except MissingEnvVar as e:
        print(e)
        usage()
        exit(-1)

    print('Run asterisk daemon...')
    subprocess.call(('/usr/sbin/asterisk', '-f'))


if __name__ == '__main__':
    main()
