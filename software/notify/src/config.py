import platform
from copy import deepcopy

from utils.exceptions import MissingEnvVarException
from utils.functions import config_generator, usage

# List of environment (env_vars, local_name) to be loaded in the config
REQUIRED_ENV_VARS = [('ARI_URL', 'ARI_URL'),
                     ('ARI_USERNAME', 'ARI_USERNAME'),
                     ('ARI_PASSWORD', 'ARI_PASSWORD'),
                     ('REDIS_HOST', 'REDIS_HOST'),
                     ('REDIS_PORT', 'REDIS_PORT'),
                     ('REDIS_PASSWORD', 'REDIS_PASSWORD'),
                     ]

# List of (env_var, local_name, default_value) of environment variables to be loaded in the config
# with a certain default value
OPTIONAL_ENV_VARS = [('APP_NAME', 'APP_NAME', platform.node()),
                     ('DEFAULT_SIP', 'DEFAULT_SIP', None),
                     ('REDIS_PREFIX', 'REDIS_PREFIX', 'calls_'),
                     ('CALL_TIMEOUT', 'CALL_TIMEOUT', 900),
                     ('MONITORING_ADAPTER_HOST', 'MONITORING_ADAPTER_HOST', 'monitoring_adapter'),
                     ('MONITORING_ADAPTER_PREFIX', 'MONITORING_ADAPTER_PREFIX', 'eu.beia.switch'),
                     ]


class ConfigManager(object):
    try:
        __config = config_generator(REQUIRED_ENV_VARS, OPTIONAL_ENV_VARS)
    except MissingEnvVarException as e:
        usage(REQUIRED_ENV_VARS)
        exit(-1)

    @classmethod
    def get_as_dict(cls):
        return deepcopy(cls.__config)
