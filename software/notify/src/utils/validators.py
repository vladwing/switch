from functools import wraps

from flask import request

from utils.exceptions import RestException


def validate_json(f):
    """
    Flask view function decorator. Adds decoded_json param to the function.
    @return: The view if request contains valid body JSON; Raises RestException otherwise.
    """

    @wraps(f)
    def wrapper(*args, **kw):
        from werkzeug.exceptions import BadRequest
        try:
            decoded_json = request.json
        except BadRequest as e:
            raise RestException(400, 'Bad Request', 'Payload should be valid JSON.')

        return f(decoded_json=decoded_json, *args, **kw)

    return wrapper



def validate_originate_request(f):
    """
    Flask view function decorator. Validates if the view is called with valid originate parameters as
    decoded_json param.
    @return: The view if request contains a valid request; Raises RestException otherwise.
    """

    def check_has_parameters(dictionary, keys):
        for k in keys:
            if not dictionary.has_key(k):
                long_description = '{} mandatory parameter is missing.'.format(k)
                raise RestException(400, 'Bad Request', long_description)

    @wraps(f)
    def wrapper(*args, **kw):
        check_has_parameters(kw['decoded_json'], ('extension', 'file'))

        return f(*args, **kw)

    return wrapper
