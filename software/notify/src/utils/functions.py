import socket
import os
from .exceptions import MissingEnvVarException

def usage(required_vars):
    env_variables = [k for k, l in required_vars]
    print('Usage for {}: {} environment variables are required to run this script.'.format(os.path.basename(__file__),
                                                                                           env_variables,
                                                                                           ))


def get_env_var(var_name, default=None, silent=False):
    result = os.environ.get(var_name, default)
    if not result and not silent:
        raise MissingEnvVarException(var_name)
    return result


def get_local_hostname():
    return socket.gethostname()


def get_local_ip():
    return socket.gethostbyname(get_local_hostname())


def get_metric_key(probe_name, key_name, prefix='eu.beia.switch'):
    return ".".join(map(str, [prefix, # prefix
        get_local_hostname(), # agent hostname
        get_local_ip().replace('.', '-'), # agent ip
        probe_name, # probe name
        key_name] # metric name
        ))


def config_generator(required_env_vars, optional_env_vars):
    config = {}
    for env_var, local_name in required_env_vars:
        config[local_name] = get_env_var(env_var)

    for env_var, local_name, default_value in optional_env_vars:
        config[local_name] = get_env_var(env_var, default=default_value, silent=True)

    return config


def get_call_redis_key(call_id):
    """
    Returns the redis key for a given call_id
    :param call_id: the call id
    :return: the redis key to be used for storing the call object
    """
    from config import ConfigManager
    config = ConfigManager.get_as_dict()
    return '{prefix}{call_id}'.format(prefix=config['REDIS_PREFIX'],
                                      call_id=call_id)


def get_call_object(call_id):
    """
    Returns the OutgoingCall object from the Redis returned
    by RedisConnector. If the call does not exist, the function returns None.
    :param call_id: The call ID.
    """
    from utils.connectors import RedisConnector
    import jsonpickle
    r = RedisConnector.get_connection()
    pickled_object = r.get(get_call_redis_key(call_id))
    if pickled_object:
        return jsonpickle.decode(pickled_object)


def get_call_status(call_id):
    """
    Return a tuple of (status_code, status) from the OutgoingCall
    stored in the Redis instance of RedisConnector with the given
    call_id.
    :param call_id: The ID of the call.
    """
    call_obj = get_call_object(call_id)
    return call_obj.status_code, call_obj.status


def update_call_object(call_id, call_object):
    from utils.connectors import RedisConnector
    import jsonpickle
    r = RedisConnector.get_connection()
    redis_key = get_call_redis_key(call_id)
    json_encoded = jsonpickle.encode(call_object)
    r.set(redis_key, json_encoded)


def update_call_status(call_id, status_code, status):
    # Get old object
    call_obj = get_call_object(call_id)

    # Update values
    call_obj.status_code = status_code
    call_obj.status = status

    # Update redis
    update_call_object(call_id, call_obj)


def get_channel_to_call_id_key(channel_id):
    from config import ConfigManager
    config = ConfigManager.get_as_dict()
    return '{}_ctc_{}'.format(config['REDIS_PREFIX'], channel_id)


def add_channel_to_call_id(call_id, channel_id, ex=7200):
    # Add channel to call_id reference
    from connectors import RedisConnector

    r = RedisConnector.get_connection()
    redis_key = get_channel_to_call_id_key(channel_id)
    r.set(redis_key, call_id, ex=ex)


def get_call_id_by_channel(channel):
    """
    Returns the call_id associated with a given channel
    """
    from connectors import RedisConnector
    r = RedisConnector.get_connection()
    return r.get(get_channel_to_call_id_key(channel))
