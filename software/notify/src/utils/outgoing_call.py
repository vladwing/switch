class OutgoingCall(object):
    def __init__(self, timestamp, call_id, playback_file, sip_provider, called_extension, status_dict):
        self.timestamp = timestamp
        self.call_id = call_id
        self.status = status_dict['status']
        self.playback_file = playback_file
        self.called_extension = called_extension
        self.sip_provider = sip_provider
        self.status_code = status_dict['status_code']

    def update_status(self, status_dict, online=True):
        from utils.functions import update_call_status
        self.status = status_dict['status']
        self.status_code = status_dict['status_code']

        if online:
            update_call_status(self.call_id, self.status_code, self.status)

    def get_as_dict(self):
        return {'id': self.call_id,
                'timestamp': self.timestamp,
                'file': self.playback_file,
                'status': self.status,
                'status_code': self.status_code
                }

    def save(self):
        from utils.functions import update_call_object
        update_call_object(self.call_id, self)


class CallStatusesContainerType(type):
    # List of (key, status_string, status_code) tuples
    call_status_config = [('initiated', 'initiated', 1),
                          ('in_playback', 'playback', 2),
                          ('success', 'finished', 0),
                          ('failed', 'failed', -1),
                          ('timeout', 'timeout', -2),
                          ]
    statues_dict = None

    @classmethod
    def __build_dict(mcs):
        mcs.statues_dict = {key: {'status': status, 'status_code': status_code} for (key, status, status_code) in
                            mcs.call_status_config}

    @classmethod
    def __getattribute__(mcs, item):
        import copy
        if not mcs.statues_dict:
            mcs.__build_dict()

        if mcs.statues_dict.has_key(item):
            return copy.deepcopy(mcs.statues_dict[item])
        else:
            raise AttributeError(item)


class CallStatuses():
    __metaclass__ = CallStatusesContainerType
