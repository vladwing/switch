class MissingEnvVarException(Exception):
    def __init__(self, variable):
        self.required_var = variable

    def __str__(self):
        return '{} environment variable is required and not set.'.format(self.required_var)


class RestException(Exception):
    def __init__(self, http_code, short_description, long_description=None):
        self.http_code = http_code
        self.short_description = short_description
        self.long_description = long_description

    def get_as_dict(self):
        result = {'error': {'http_code': self.http_code,
                            'short_description': self.short_description}}

        if self.long_description:
            result['error']['long_description'] = self.long_description

        return result


class ConnectorException(Exception):
    def __init__(self, connection_name):
        self.connection_name = connection_name

    def __str__(self):
        print('{} is unavailable.'.format(self.connection_name))
