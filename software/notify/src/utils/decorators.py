import logging
import time
from functools import wraps

logger = logging.getLogger(__name__)


def pass_start_time(f):
    """
    Decorator. Adds mon_start_time parameter to the decorated function
    :return: The same value as the decorated function.
    """

    @wraps(f)
    def wrapper(*args, **kw):
        return f(mon_start_time=time.time(), *args, **kw)

    return wrapper


def redis_connection_exception_handler(f):
    """
    Flask view function decorator. Intercepts redis Connection exceptions and raises RestException instead.
    @return: The view decorated view if no redis connection exception is raised. RestException raised instead.
    """

    from .exceptions import RestException
    import redis

    @wraps(f)
    def wrapper(*args, **kw):
        try:
            return f(*args, **kw)
        except redis.ConnectionError as e:
            logger.exception('Redis ConnectionError')
            raise RestException(500, 'Internal Server Error', 'Failed to communicate with Redis')

    return wrapper
