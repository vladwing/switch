import ari
import redis

from config import ConfigManager
from utils.exceptions import ConnectorException

config = ConfigManager.get_as_dict()


class RedisConnector(object):
    instance = None

    @classmethod
    def __get_new_connection(cls):
        return redis.StrictRedis(host=config['REDIS_HOST'],
                                 port=config['REDIS_PORT'], db=0,
                                 password=config['REDIS_PASSWORD'])

    @classmethod
    def get_connection(cls, silent=False):
        if not cls.instance:
            cls.instance = cls.__get_new_connection()

        if not silent and not cls.instance:
            raise ConnectorException('Redis')

        return cls.instance


class AsteriskConnector(object):
    instance = None

    @classmethod
    def __get_new_connection(cls):
        from requests.exceptions import ConnectionError
        try:
            return ari.connect(config['ARI_URL'], config['ARI_USERNAME'], config['ARI_PASSWORD'])
        except ConnectionError as e:
            return None

    @classmethod
    def get_connection(cls, silent=False):
        if not cls.instance:
            cls.instance = cls.__get_new_connection()

        if not silent and not cls.instance:
            raise ConnectorException('Asterisk')

        return cls.instance
