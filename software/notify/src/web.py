import json
import time
import atexit
import resource
from datetime import datetime
import logging
from apscheduler.schedulers.background import BackgroundScheduler
from uuid import uuid4
from flask import Flask, jsonify, current_app, g
from requests.exceptions import HTTPError
from statsd import StatsClient
from config import ConfigManager
from utils.connectors import AsteriskConnector, RedisConnector
from utils.decorators import pass_start_time, redis_connection_exception_handler
from utils.exceptions import RestException
from utils.functions import update_call_object, get_call_object, get_metric_key
from utils.outgoing_call import OutgoingCall, CallStatuses
from utils.validators import validate_json, validate_originate_request

app = Flask(__name__)

# Need this to wire Gunicorn logging inside our app
if __name__ != '__main__':
    gunicorn_logger = logging.getLogger('gunicorn.error')
    app.logger.handlers = gunicorn_logger.handlers
    app.logger.setLevel(gunicorn_logger.level)

def init_app(app):
    app.logger.info("Initializing application")
    app.web_config = config = ConfigManager.get_as_dict()
    app.statsd = statsd = StatsClient(config['MONITORING_ADAPTER_HOST'], port=8125)
    app.sched = sched = BackgroundScheduler(timezone='UTC')
    sched.start()

    @sched.scheduled_job('interval', seconds=10)
    def run_monitoring_agent_step():
        app.logger.info("Interval callback")
        rusage = resource.getrusage(resource.RUSAGE_SELF)
        statsd.gauge(get_metric_key('MemoryProbe', 'memMaxResidentSetSize'), rusage.ru_maxrss)
        rusage = resource.getrusage(resource.RUSAGE_CHILDREN)
        statsd.gauge(get_metric_key('MemoryProbe', 'memMaxResidentChildSetSize'), rusage.ru_maxrss)

    run_monitoring_agent_step()

def clean_app(app):
    app.sched.shutdown(wait=False)

init_app(app)
atexit.register(lambda: clean_app(app))


@app.route('/')
def index():
    return 'Hello world'


@app.route('/originate', methods=['post'])
@pass_start_time
@validate_json
@validate_originate_request
@redis_connection_exception_handler
def originate(mon_start_time=None, decoded_json=None):
    current_app.statsd.gauge(get_metric_key('APIStats', 'HitCount'), 1, delta=True)
    current_app.statsd.gauge(get_metric_key('CallStats', 'OriginatedCalls'), 1, delta=True)

    redis = RedisConnector.get_connection()
    ari = AsteriskConnector.get_connection()

    sip_provider = decoded_json.get('sip_provider') or current_app.web_config['DEFAULT_SIP']
    if not sip_provider:
        raise RestException(400, 'Bad Request',
                            long_description='sip_provider parameter is missing & no default is defined')

    # Sanity -- check if Stasis app is registered with Asterisk
    check_worker_is_registered()

    # Generate call_id and redis_key
    call_id = uuid4()

    # Create the Call object and add it to the redis
    call_object = OutgoingCall(mon_start_time, call_id, decoded_json['file'], sip_provider, decoded_json['extension'],
                               CallStatuses.initiated)
    update_call_object(call_id, call_object)

    # Initiate call
    called_endpoint = 'SIP/{sip_peer}/{extension}'.format(sip_peer=sip_provider,
                                                          extension=decoded_json['extension'])

    ari.channels.originate(endpoint=called_endpoint,
                           app=current_app.web_config['APP_NAME'],
                           appArgs=[str(call_id), decoded_json['file']])

    # Return the async answer
    return jsonify(call_object.get_as_dict())


@app.route('/call/<call_id>', methods=['GET'])
@redis_connection_exception_handler
def get_call(call_id):
    current_app.statsd.gauge(get_metric_key('APIStats', 'HitCount'), 1, delta=True)
    call_object = get_call_object(call_id)

    if not call_object:
        raise RestException(404, 'Call not found')

    if time.time() - call_object.timestamp > current_app.web_config['CALL_TIMEOUT'] and call_object.status_code > 0:
        call_object.update_status(CallStatuses.timeout)

    return jsonify(call_object.get_as_dict())


def check_worker_is_registered():
    """
    Checks whatever the Stasis app is registered with Asterisk.
    Raise 500 RestException if it is not or other HTTPError is raised.
    :return: None
    """
    ari = AsteriskConnector.get_connection()
    try:
        ari.applications.get(applicationName=current_app.web_config['APP_NAME'])
    except HTTPError as e:
        # Warning! e.errno is not the same. Do not fall in this trap!
        if e.response.status_code == 404:
            raise RestException(500, 'Internal Server Error', 'Asterisk Worker is not registered with the PBX.')
        else:
            raise RestException(500, 'Internal Server Error', 'There is a problem communicating with Asterisk.')


@app.errorhandler(RestException)
def rest_exception_handler(e):
    return __create_json_response_from_dict(e.http_code, e.get_as_dict())


@app.errorhandler(500)
def internal_error_handler(e):
    # That comes to be strange, but raise RestException won't work.
    current_app.logger.exception('Internal Server Error')
    return __create_json_response_from_dict(500, RestException(500, 'Internal Server Error').get_as_dict())


def __create_json_response_from_dict(http_status_code, dictionary):
    return current_app.response_class(
        response=json.dumps(dictionary),
        status=http_status_code,
        mimetype='application/json',
    )


if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
