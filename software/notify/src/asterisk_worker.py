#!/usr/bin/env python
import logging
import uuid

import ari

from config import ConfigManager
from utils.functions import add_channel_to_call_id, get_call_object, get_call_id_by_channel
from utils.outgoing_call import CallStatuses

config = ConfigManager.get_as_dict()

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.ERROR)

client = ari.connect(config['ARI_URL'],
                     config['ARI_USERNAME'],
                     config['ARI_PASSWORD'])


def stasis_end_cb(channel, ev):
    """Handler for StasisEnd event"""
    logger.info("Channel {} just left our application".format(channel.json.get('id')))
    call_id = get_call_id_by_channel(channel.json.get('id'))
    call_object = get_call_object(call_id)

    # Playback failed
    if not call_object.status_code == 0:
        call_object.update_status(CallStatuses.failed)


def stasis_start_cb(channel_obj, ev):
    """Handler for StasisStart event"""

    def playback_finished(playback, ev, call_id=None):
        from requests.exceptions import HTTPError
        """Callback when the playback is over"""
        target_uri = playback.json.get('target_uri')
        channel_id = target_uri.replace('channel:', '')
        try:
            channel = client.channels.get(channelId=channel_id)
            channel.hangup()
        except HTTPError as e:
            if e.response.status_code == 404:
                # Called person already hanged up
                pass
            else:
                raise e

        logger.info('[{}] - Playback Finished!'.format(call_id))
        call_object = get_call_object(call_id)
        call_object.update_status(CallStatuses.success)

    # Get call details
    channel = channel_obj.get('channel')
    call_id = ev['args'][0]
    playback_file_name = ev['args'][1]
    channel_id = channel.json.get('id')

    logger.info('[{}] - Call started!'.format(call_id))

    # Get outgoing call instance
    call_object = get_call_object(call_id)

    # Add channel to call_id reference
    add_channel_to_call_id(call_id, channel_id)

    # Start playback
    playback_id = str(uuid.uuid4())
    playback = channel.playWithId(playbackId=playback_id,
                                  media='sound:{}'.format(playback_file_name))

    logger.info('[{}] - Playback Started!'.format(call_id))
    call_object.update_status(CallStatuses.in_playback)
    playback.on_event('PlaybackFinished',
                      lambda *args, **kwargs:
                      playback_finished(call_id=call_id, *args, **kwargs))


if __name__ == "__main__":
    client.on_channel_event('StasisStart', stasis_start_cb)
    client.on_channel_event('StasisEnd', stasis_end_cb)

    print('<--- Starting Asterisk Worker with name {} --->'.format(config['APP_NAME']))
    client.run(apps=config['APP_NAME'])
