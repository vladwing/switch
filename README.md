# Beia contribution to Switch project

----
## VPN

Folder [`/util/vpn`](util/vpn) contains configuration and scripts for accessing the docker machine VPN using [tinc](https://www.tinc-vpn.org/).

----
## Docker
The [`/docker`](docker) folder contains configuration using `docker` and `docker-compose` for different services included in this setup. Right now the following are configured and setup(work is still in progress):
 
- [`Telegreen`](docker/telegreen) - this is an dockerized setup for the application that runs on [http://telegreen.beia.ro/](http://telegreen.beia.ro)

----
## Software

- [`Telegreen`](software/telegreen) - this is an PHP aplication that connects to a [M2M/IoT A850 Adcon Gateway](http://82.78.81.167/) system and pulls data to be presented in a web interface.

